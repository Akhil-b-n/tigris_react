export default function Header(){
    return (
        <header className='border-b-4 border-blue-500'>
        <div className='container flex flex-row justify-between items-center mx-auto px-3 my-4  '>
          <a href="#"><h1 className='font-bold p1'>Tigris</h1></a>
          {/*nav section*/}
        <nav>
        <div>
          {/*menu icon*/}
        <span className="material-symbols-outlined p1 icon md:hidden">
          pets
        </span>
        {/*menu list */}
        <ul className='hidden flex flex-col items-center justify-center gap-5 text-xl md:flex md:flex-row md:justify-between md:items-center menu '>
          <li>Home</li>
          <li>About</li>
          <li>Services</li>
          <li>Contact</li>
        </ul>
        </div>
        </nav>
        </div>
       </header>  
    )
}
